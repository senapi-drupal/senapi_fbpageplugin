Drupal.behaviors.senapi_fbpageplugin = {
    attach: function (context, settings) {
        if (context !== document) {
            // AJAX request.
            return;
        }
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = '//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.2';
            if (settings.fbpageplugin_app_id) {
                js.src += '&appId=' + settings.fbpagepluginAppId;
            }
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk', settings));
    }
};