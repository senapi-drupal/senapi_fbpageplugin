<?php

namespace Drupal\senapi_fbpageplugin\Plugin\Block;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SenapiFbpagepluginBlock
 *
 * @Block(
 *     id = "senapi_fbpageplugin_block",
 *     admin_label = @Translation("Senapi Facebook Page Plugin"),
 *     category = @Translation("Senapi Facebook Page Plugin")
 * )
 */
class SenapiFbpagepluginBlock extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build() {
        $config = $this->getConfiguration();

        $render['root-div'] = [
            '#type' => 'container',
            '#attributes' => [
                'id' => ['fb-root']
            ]
        ];

        $render['block'] = [
          '#type' => 'container',
          '#attributes' => [
              'class' => ['fb-page'],
              'data-href' => $config['url'],
              'data-tabs' => 'timeline',
              'data-width' => $config['width'],
              'data-height' => $config['height'],
              'data-small-header' => FALSE,
              'data-adapt-container-width' => TRUE,
              'data-hide-cover' => FALSE,
              'data-show-facepile' => TRUE,
          ]
        ];

        $render['block']['child'] = [
          '#type' => 'link',
          '#title' => $config['title'],
          '#href' => $config['url'],
          '#prefix' => '<blockquote cite="' . $config['url'] . '" class="fb-xfbml-parse-ignore"><a href="' . $config['url'] . '">'  . $config['title'] . '</a>',
          '#suffix' => '</blockquote>',
        ];

        $render['#attached']['library'][] = 'senapi_fbpageplugin/fbpageplugin';
        $render['#attached']['drupalSettings']['fbpagepluginAppId'] = $config['app_id'];

        return $render;
    }

    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state) {
        $config = $this->getConfiguration();

        $form['senapi_fbpageplugin_settings'] = [
            '#type' => 'details',
            '#title' => $this->t('Display options'),
            '#open' => TRUE
        ];

        $form['senapi_fbpageplugin_settings']['url'] = [
            '#type' => 'url',
            '#title' => $this->t('Facebook Page URL'),
            '#default_value' => $config['url'],
            '#description' => $this->t('Enter the Facebook Page URL Ejem: https://www.facebook.com/senapi.bolivia'),
            '#required' => TRUE,
        ];

        $form['senapi_fbpageplugin_settings']['app_id'] = [
            '#type' => 'textfield',
            '#title' => $this->t('App ID'),
            '#default_value' => $config['app_id'],
        ];

        $form['senapi_fbpageplugin_settings']['hide_header'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Hide cover photo in the header'),
            '#default_value' => $config['hide_header']
        ];

        $form['senapi_fbpageplugin_settings']['stream'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Show posts from the Pages timeline'),
            '#default_value' => $config['stream'],
        ];

        $form['senapi_fbpageplugin_settings']['show_faces'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Show profile photos when friends like this'),
            '#default_value' => $config['show_faces']
        ];

        $form['senapi_fbpageplugin_settings']['title'] = [
            '#type' => 'textfield',
            '#title' => $this->t('iFrame title attributes'),
            '#default_value' => $config['title'],
            '#description' => $this->t('The value of the title attribute.'),
            '#required' => TRUE,
        ];

        $form['senapi_fbpageplugin_settings']['width'] = [
            '#type' => 'number',
            '#title' => $this->t('Width'),
            '#default_value' => $config['width'],
            '#description' => $this->t('The width of the Facebook Page Plugin. Must be a number between 180 an 500, limits icluded.'),
            '#min' => 180,
            '#max' => 500,
            '#required' => TRUE,
        ];

        $form['senapi_fbpageplugin_settings']['height'] = [
            '#type' => 'number',
            '#title' => $this->t('Height'),
            '#default_value' => $config['height'],
            '#description' => $this->t('The height of the plugin in pixels. Must be a number bigger than 70.'),
            '#required' => TRUE,
        ];

        $form['senapi_fbpageplugin_settings']['hide_cta'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Hide the custom call to action button (if available)'),
            '#default_value' => $config['hide_cta']
        ];

        $form['senapi_fbpageplugin_settings']['small_header'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Use the small header instead'),
            '#default_value' => $config['small_header']
        ];

        $form['senapi_fbpageplugin_settings']['adapt_container_width'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Try to fit inside the container width'),
            '#default_value' => $config['adapt_container_width']
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state) {
        $displaySettings = $form_state->getValue('senapi_fbpageplugin_settings');
        foreach ($displaySettings as $key => $value) {
            $this->setConfigurationValue($key, $value);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration() {
        return [
            'url' => '',
            'app_id' => '',
            'hide_header' => '',
            'stream' => '',
            'show_faces' => '',
            'title' => '',
            'width' => '',
            'height' => '',
            'hide_cta' => '',
            'small_cta' => '',
            'small_header' => '',
            'adapt_container_width' => '',
        ];
    }
}